#Author: Iurii Kolochkov
#Email: iuriikolochkov@gmail.com

This project uses djangorestframework. The project has only single app - 'service'.

Resources defined here, their fields/endpoints (and their corresponding urls) are:

'Player' with following fields:

    vine_id - required, integer field;
    image_url - required;
    pro - boolean field;
    nmbr_coins - How many coins the user has earned, integer field;
    nmbr_coins_pfollow - Coins per follow, integer field;
    coins_spent_today - The number of coins they have spent today, integer field;
    coins_spent_limit - integer field;
    date_added - datetime field, generated automatically
    
The URI of this resource is http://127.0.0.1:8000/players/{vine_id}

'Promoted Player' with following fields:
    vine_id - required, integer field;
    image_url - required;
    followers_remain - integer field;
    date_promoted - - datetime field, generated automatically;
    
The URI of this resource is http://127.0.0.1:8000/ppls/{vine_id}

'Promoted Video' with following fields:
    
    video_id - required, integer field, primary key
    vine_id - required, integer field
    video_url - requred
    thumbnail_url - requred
    number_of_likes - requred
    
The URI of this resource is http://127.0.0.1:8000/videos/{video_id}
    
All POST, PATCH, DELETE requests to those resources should contain vine_id, timestamp and token which is SHA1 encrypted string.
For testing purposes you may find usefull this tool: http://www.movable-type.co.uk/scripts/sha1.html
When creating token, order matters. Corresponding function (utils.tokenize) accepts three POSITIONAL arguments: timestamp, privateKey, vine_id, so bear this in mind.

'PlayerFollow' and 'Likes' resources don't have representation through API and can be easily explored in admin page.

Examples of all kinds of requests can be found in example_requests.txt