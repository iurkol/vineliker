# VineLiker views.py
from random import sample, seed
from service.models import Player, PromotedPlayer, PromotedVideos, Likes, PlayerFollow
from service.serializers import PlayerSerializer, PromotedPlayerSerializer, PromotedVideosSerializer, LikesSerializer, PlayerFollowSerializer
from backend import settings
from utils import tokenize
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response


# PLAYER Views #######################################
@api_view(['GET', 'POST'])
def player_list(request):
    """
    List all players (GET), or create a new player (POST with token check)
    Fields:
    vine_id
    image_url
    pro
    coins_spent_today
    timestamp
    token
    All fields are required when using POST
    """
    if request.method == 'GET':
        players = Player.objects.all()
        serializer = PlayerSerializer(players, many=True)
        response = Response(serializer.data)

    elif request.method == 'POST':
        try:
            token = tokenize(request.DATA['timestamp'], settings.privateKey, request.DATA['vine_id'])
            assert token == request.DATA['token']
            serializer = PlayerSerializer(data=request.DATA)
            if serializer.is_valid():
                serializer.save()
                response = Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                response = Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except KeyError:
            response = Response(data=({"error": {"message": "You are not authorized; wrong request data", "code": 401}}), status=status.HTTP_401_UNAUTHORIZED, content_type = 'application/json')
        except AssertionError:
            response = Response(data=({"error": {"message": "You are not authorized; token does not match", "code": 401}}), status=status.HTTP_401_UNAUTHORIZED, content_type = 'application_json')
    return response


@api_view(['GET', 'PATCH', ])
def player_detail(request, pk):
    """
    Retrieve, update or delete a player instance.
    """
    try:
        player = Player.objects.get(pk=pk)
    except Player.DoesNotExist:
        response = Response(data='Player not found', status=status.HTTP_404_NOT_FOUND)  # Get out of the view function, return 404 error
        return response
    if request.method == 'GET':  # Player with this pk has been found
        serializer = PlayerSerializer(player)
        response = Response(serializer.data)

    elif request.method == 'PATCH':
        try:
            token = tokenize(request.DATA['timestamp'], settings.privateKey, request.DATA['vine_id'])
            assert token == request.DATA['token']
            serializer = PlayerSerializer(player, data=request.DATA, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except KeyError:
            response = Response(data=({"error": {"message": "You are not authorized; wrong request data", "code": 401}}), status=status.HTTP_401_UNAUTHORIZED, content_type = 'application/json')
            return response
        except AssertionError:
            response = Response(data=({"error": {"message": "You are not authorized; token does not match", "code": 401}}), status=status.HTTP_401_UNAUTHORIZED, content_type = 'application_json')
            return response
    return response


# PROMOTED PLAYER Views #####################################
@api_view(['GET', 'POST'])
def promoted_player_list(request):
    """
    List all promoted players, or create a new promoted player
    """

    if request.method == 'GET':
        # 3 randomly-selected users:
        try:
            promoted_players = PromotedPlayer.objects.random()
        except ValueError:
            return Response(data={}, content_type='application/json')
        serializer = PromotedPlayerSerializer(promoted_players, many=True)
        response = Response(serializer.data)

    elif request.method == 'POST':
        try:
            token = tokenize(request.DATA['timestamp'], settings.privateKey, request.DATA['vine_id'])
            assert token == request.DATA['token']
            request_values = request.DATA.copy()
            # Change already existed promoted player with post
            try:
                promoted_player = PromotedPlayer.objects.get(vine_id=request_values['vine_id'])
                promoted_player.followers_remain = int(request_values['followers_remain'])
                promoted_player.save()
                player = Player.objects.get(vine_id=request_values['vine_id'])
                fee = (int(request_values['followers_remain']) * 10)
                if player.nmbr_coins < fee:
                    return Response(data='Not enough coins!')
                player.nmbr_coins -= fee
                player.save()
                return Response(request_values)
            except PromotedPlayer.DoesNotExist:
                player = Player.objects.get(vine_id=request.DATA['vine_id'])
                # Grab player's image_url Remember that request.DATA is immutable, so we must copy it
                request_values.update({'image_url': player.image_url})
                fee = (int(request_values['followers_remain']) * 10)
                if player.nmbr_coins < fee:
                    return Response(data='Not enough coins!')
                serializer = PromotedPlayerSerializer(data=request_values)
                if serializer.is_valid():
                    player.nmbr_coins -= fee
                    player.save()
                    serializer.save()
                    response = Response(serializer.data, status=status.HTTP_201_CREATED)
                else:
                    response = Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Player.DoesNotExist:
            response = Response(data='Player with this "vine_id" does not exist', status=status.HTTP_404_NOT_FOUND)
        except KeyError:
            response = Response(data=({"error": {"message": "You are not authorized; wrong request data", "code": 401}}), status=status.HTTP_401_UNAUTHORIZED, content_type = 'application/json')
        except AssertionError:
            response = Response(data=({"error": {"message": "You are not authorized; token does not match", "code": 401}}), status=status.HTTP_401_UNAUTHORIZED, content_type = 'application_json')
    return response


@api_view(['GET', 'PATCH', ])
def promoted_player_detail(request, pk):
    """
    Retrieve, update or delete a promoted player instance.
    """
    try:
        promoted_player = PromotedPlayer.objects.get(pk=pk)
    except PromotedPlayer.DoesNotExist:
        response = Response(data='Promoted player not found', status=status.HTTP_404_NOT_FOUND)  # Get out of the view function, return 404 error
        return response
    if request.method == 'GET':  # Player with this pk has been found
        serializer = PromotedPlayerSerializer(promoted_player)
        response = Response(serializer.data)

    elif request.method == 'PATCH':
        try:
            token = tokenize(request.DATA['timestamp'], settings.privateKey, request.DATA['vine_id'])
            assert token == request.DATA['token']
            serializer = PromotedPlayerSerializer(promoted_player, data=request.DATA, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except KeyError:
            response = Response(data=({"error": {"message": "You are not authorized; wrong request data", "code": 401}}), status=status.HTTP_401_UNAUTHORIZED, content_type = 'application/json')
            return response
        except AssertionError:
            response = Response(data=({"error": {"message": "You are not authorized; token does not match", "code": 401}}), status=status.HTTP_401_UNAUTHORIZED, content_type = 'application_json')
            return response
    return response


# VIDEO Views #######################################
@api_view(['GET', 'POST'])
def video_list(request):
    """
    List all videos, or create a new video
    """
    if request.method == 'GET':
        # 3 randomly-selected videos:
        try:
            videos = sample(PromotedVideos.objects.all(), 3)
            # Convert integers into strings
            for x in videos:
                x.video_id = str(x.video_id)
        except ValueError:
            return Response(data=[], content_type='application/json')
        serializer = PromotedVideosSerializer(videos, many=True)
        response = Response(serializer.data)

    elif request.method == 'POST':
        try:
            token = tokenize(request.DATA['timestamp'], settings.privateKey, request.DATA['vine_id'])
            assert token == request.DATA['token']
            #serializer = PromotedVideosSerializer(data=request.DATA)
            player = Player.objects.get(vine_id=request.DATA['vine_id'])
            fee = int(request.DATA['number_of_likes']) * 10
            if player.nmbr_coins < fee:
                return Response(data='Not enough coins!')
            player.nmbr_coins -= fee
            player.save()
            
            video = PromotedVideos(
                video_id = request.DATA['video_id'],
                vine_id = request.DATA['vine_id'],
                video_url = request.DATA['video_url'],
                thumbnail_url = request.DATA['thumbnail_url'],
                number_of_likes = request.DATA['number_of_likes'],
            )
            video.save()
            # Convert integers into strings
            request_values = request.DATA.copy()
            request_values['vine_id'] = str(request_values['vine_id'])
            request_values['video_id'] = str(request_values['video_id'])
            response = Response(request_values, status=status.HTTP_201_CREATED)
            #if serializer.is_valid():
            #    player.nmbr_coins -= fee
            #    player.save()
            #    serializer.save()
            #    response = Response(serializer.data, status=status.HTTP_201_CREATED)
            #else:
            #    response = Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except KeyError:
            response = Response(data=({"error": {"message": "Wrong request data", "code": 401}}), status=status.HTTP_401_UNAUTHORIZED, content_type = 'application/json')
        except AssertionError:
            response = Response(data=({"error": {"message": "You are not authorized; token does not match", "code": 401}}), status=status.HTTP_401_UNAUTHORIZED, content_type = 'application_json')
        except Player.DoesNotExist:
            response = Response(data='Player with this "vine_id" does not exist', status=status.HTTP_404_NOT_FOUND)
    return response


@api_view(['GET', 'PATCH', ])
def video_detail(request, pk):
    """
    Retrieve, update or delete a particular video instance.
    """
    try:
        video = PromotedVideos.objects.get(pk=pk)
    except PromotedVideos.DoesNotExist:
        response = Response(data='Video not found', status=status.HTTP_404_NOT_FOUND)  # Get out of the view function, return 404 error
        return response
    if request.method == 'GET':  # Video with this pk has been found
        video.video_id = str(video.video_id)
        video.vine_id = str(video.vine_id)
        serializer = PromotedVideosSerializer(video)
        response = Response(serializer.data)

    elif request.method == 'PATCH':
        try:
            token = tokenize(request.DATA['timestamp'], settings.privateKey, request.DATA['vine_id'])
            assert token == request.DATA['token']
            serializer = PromotedVideosSerializer(video, data=request.DATA, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except KeyError:
            response = Response(data=({"error": {"message": "You are not authorized; wrong request data", "code": 401}}), status=status.HTTP_401_UNAUTHORIZED, content_type = 'application/json')
            return response
        except AssertionError:
            response = Response(data=({"error": {"message": "You are not authorized; token does not match", "code": 401}}), status=status.HTTP_401_UNAUTHORIZED, content_type = 'application_json')
            return response
    return response


# Likes View, only POST allowed #######################################
@api_view(['POST'])
def likes_view(request):
    """
    Create a new like
    """
    if request.method == 'POST':
        try:
            token = tokenize(request.DATA['timestamp'], settings.privateKey, request.DATA['liker_id'])
            assert token == request.DATA['token']
            video = PromotedVideos.objects.get(video_id=request.DATA['video_id'])
            video.number_of_likes += 1
            video.save()
            player = Player.objects.get(vine_id=request.DATA['liker_id'])
            player.nmbr_coins += 1
            player.save()
            request_values = request.DATA.copy()
            request_values.update({'owner_id': video.vine_id})
            serializer = LikesSerializer(data=request_values)
            if serializer.is_valid():
                serializer.save()
                response = Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                response = Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except KeyError:
            response = Response(data=({"error": {"message": "You are not authorized; wrong request data", "code": 401}}), status=status.HTTP_401_UNAUTHORIZED, content_type = 'application/json')
        except AssertionError:
            response = Response(data=({"error": {"message": "You are not authorized; token does not match", "code": 401}}), status=status.HTTP_401_UNAUTHORIZED, content_type = 'application_json')
        except Player.DoesNotExist:
            response = Response(data='Player with this "vine_id" does not exist', status=status.HTTP_404_NOT_FOUND)
        except PromotedVideos.DoesNotExist:
            response = Response(data='Video with this id does not exist', status=status.HTTP_404_NOT_FOUND)
    return response


# Follow View, only POST allowed #######################################
@api_view(['POST'])
def follows_view(request):
    """
    Create a new follow instance.
    Only POST is allowed.
    For tokenize we use 'follower_id', not 'vine_id' here, as defined in service.models
    Note: player, follower and followee must exist
    Fields:
    follower_id
    followee_id
    timestamp
    token
    Those fields are all required
    """
    if request.method == 'POST':
        try:  # Follower exists?
            player = Player.objects.get(vine_id=request.DATA['follower_id'])
            player.nmbr_coins += player.nmbr_coins_pfollow
            player.save()

            promoted_player = PromotedPlayer.objects.get(vine_id=request.DATA['followee_id'])
            promoted_player.followers_remain -= 1
            promoted_player.save()
            if promoted_player.followers_remain <= 0:
                promoted_player.delete()
        except Player.DoesNotExist:
            return Response(data=({"error": {"message": "Follower with this id not found", "code": 404}}), status=status.HTTP_404_NOT_FOUND, content_type = 'application/json')
        except PromotedPlayer.DoesNotExist:
            return Response(data=({"error": {"message": "Followee (promoted_player) with this id not found", "code": 404}}), status=status.HTTP_404_NOT_FOUND, content_type = 'application/json')
        try:  # Followee exists?
            player = Player.objects.get(vine_id=request.DATA['followee_id'])
        except Player.DoesNotExist:
            return Response(data=({"error": {"message": "Followee with this id not found", "code": 404}}), status=status.HTTP_404_NOT_FOUND, content_type = 'application/json')
        try:
            token = tokenize(request.DATA['timestamp'], settings.privateKey, request.DATA['follower_id'])
            assert token == request.DATA['token']
            serializer = PlayerFollowSerializer(data=request.DATA)
            if serializer.is_valid():
                serializer.save()
                response = Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                response = Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except KeyError:
            response = Response(data=({"error": {"message": "You are not authorized; wrong request data", "code": 401}}), status=status.HTTP_401_UNAUTHORIZED, content_type = 'application/json')
        except AssertionError:
            response = Response(data=({"error": {"message": "You are not authorized; token does not match", "code": 401}}), status=status.HTTP_401_UNAUTHORIZED, content_type = 'application_json')
    return response


# Upgrades #######################################
@api_view(['POST'])
def upgrades(request):
    '''
    The app will allow users to purchase upgrades. These are the following upgrades:
    10,000 coins (com.MobileXLabs.coins.tier_one)
    2,500 coins (com.MobileXLabs.coins.tier_two)
    500 coins (com.MobileXLabs.coins.tier_three)
    100 coins (com.MobileXLabs.coins.tier_four)
    Pro Upgrade (com.MobileXLabs.pro)
    '''
    player = Player.objects.get(vine_id=request.DATA.get('vine_id'))
    try:
        token = tokenize(request.DATA['timestamp'], settings.privateKey, request.DATA['vine_id'])
        assert token == request.DATA['token']
        if request.DATA['item_identifier'] == 'com.MobileXLabs.coins.tier_one':
            player.nmbr_coins += 10000
        elif request.DATA['item_identifier'] == 'com.MobileXLabs.coins.tier_two':
            player.nmbr_coins += 2500
        elif request.DATA['item_identifier'] == 'com.MobileXLabs.coins.tier_three':
            player.nmbr_coins += 500
        elif request.DATA['item_identifier'] == 'com.MobileXLabs.coins.tier_four':
            player.nmbr_coins += 100
        elif request.DATA['item_identifier'] == 'com.MobileXLabs.pro':
            player.pro = True
        player.save()
        return Response(data=request.DATA)
    except Player.DoesNotExist:
        return Response(data=({"error": {"message": "User with this id not found", "code": 404}}), status=status.HTTP_404_NOT_FOUND, content_type = 'application/json')
    except KeyError:
        return Response(data=({"error": {"message": "Wrong request data", "code": 401}}), status=status.HTTP_401_UNAUTHORIZED, content_type = 'application/json')
    except AssertionError:
        return Response(data={"error": {"message": "You are not authorized; token does not match", "code": 401}}, status=status.HTTP_401_UNAUTHORIZED, content_type='application_json')
