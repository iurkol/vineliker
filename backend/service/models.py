#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# Vine models

from django.db import models
from random import randint, Random, seed
from django.db.models import Count


class PromotedPlayerManager(models.Manager):

    def random(self):
        count = self.count()
        return [self.all()[Random().randint(0, count - 1)], self.all()[Random().randint(0, count - 1)], self.all()[Random().randint(0, count - 1)]]


class Player(models.Model):
    # Player's vine_id, an integer
    vine_id = models.BigIntegerField('User\'s vine_id', unique=True, primary_key=True)
    #vine_id = models.CharField(max_length = 128, help_text = 'User\'s vine_id', unique=True)
    # URL of user's  profile picture
    image_url = models.URLField("URL of user's  profile picture")
    # If the user upgrades to pro, this will be True
    pro = models.BooleanField(verbose_name="User is pro")
    # How many coins the user has earned
    nmbr_coins = models.BigIntegerField("How many coins the user has earned", default=0)
    # The number of coins a user is awarded per follow, 5 if they're a free user, 10 if they're pro
    nmbr_coins_pfollow = models.BigIntegerField("Coins per follow", default=5)
    # The number of coins they have spent today
    coins_spent_today = models.BigIntegerField("The number of coins they have spent today", default=0)
    # The limit on their spending, 100 if they're free users, 10,000 if they're pro users
    coins_spent_limit = models.BigIntegerField("Coin spend limit", default=100)
    # Date of creation
    date_added = models.DateTimeField(verbose_name="Added", auto_now_add=True)

    def __unicode__(self):
        return u'{0}'.format(self.vine_id)

    # Rewriting 'save' method provides default values for 'nmbr_coins_pfollow' 'coins_spent_limit' depending of player's 'pro' status

    def save(self, *args, **kwargs):
        if self.pro:
            self.nmbr_coins_pfollow = 10
            self.coins_spent_limit = 10000
        else:
            self.nmbr_coins_pfollow = 5
            self.coins_spent_limit = 100
        super(Player, self).save(*args, **kwargs)

    class Meta:
        db_table = 'player'
        verbose_name = "Player"
        verbose_name_plural = "Players"
        ordering = ['-date_added']


class PromotedPlayer(models.Model):  # When a user spends coins to get followers
    # Player's vine_id, an integer
    vine_id = models.BigIntegerField('User\'s vine_id', unique=True, primary_key=True)
    # URL of user's  profile picture
    image_url = models.URLField("URL of user's  profile picture")
    # The number of followers remaining until they are removed from the promoted list
    followers_remain = models.BigIntegerField("Followers remaining")
    # The time they promoted their account to go into the list
    date_promoted = models.DateTimeField("Promoted", auto_now_add=True)
    objects = PromotedPlayerManager()

    def __unicode__(self):
        return u'{0}'.format(self.vine_id)

    class Meta:
        db_table = 'promoted_players'
        verbose_name = "Promoted Player"
        verbose_name_plural = "Promoted Players"
        ordering = ['-date_promoted']


class PromotedVideos(models.Model):

    '''
    To store videos which have been promoted for liking
    '''
    video_id = models.BigIntegerField(verbose_name='ID of the video', primary_key=True, unique=True)
    vine_id = models.BigIntegerField(verbose_name='User\'s Vine ID', help_text='The Vine ID of the user who uploaded and promoted that video for likes')
    video_url = models.URLField('Video URL')
    thumbnail_url = models.URLField('Thumbanil URL')
    number_of_likes = models.BigIntegerField(verbose_name='Number of likes', help_text='Number of likes ordered')
    # The time video promoted
    date_promoted = models.DateTimeField("Promoted", auto_now_add=True, help_text='The date this video promoted', auto_now = True)

    def __unicode__(self):
        return u'{0}'.format(self.video_id)

    class Meta:
        db_table = 'promoted_videos'
        verbose_name = 'Promoted Video'
        verbose_name_plural = 'Promoted Videos'
        ordering = ['-date_promoted']


class PlayerFollow(models.Model):  # Store a record of which users followed others
    follow_id = models.AutoField(primary_key=True, verbose_name='ID', help_text='A unique ID for the follow', unique=True)
    # ID of person being followed
    followee_id = models.ForeignKey(Player, verbose_name="Followee ID", related_name="followee_id", help_text='ID of person being followed')
    # ID of the person following
    follower_id = models.ForeignKey(Player, verbose_name="Follower ID", related_name="follower_id", help_text='ID of person following')
    # The datetime
    timestamp_follow = models.DateTimeField(auto_now_add=True, verbose_name="Timestamp")

    def __unicode__(self):
        return u'{0}'.format(self.follow_id)

    class Meta:
        db_table = 'follows'
        ordering = ['-timestamp_follow']
        verbose_name = 'Follow'


class Likes(models.Model):

    '''
    To store a history of likes
    '''
    like_id = models.AutoField('ID of the like', primary_key=True, unique=True, help_text='A unique ID for the like')
    video_id = models.BigIntegerField(verbose_name='ID of the video', help_text='ID of the video being liked')
    owner_id = models.BigIntegerField(verbose_name='Owner ID', help_text='ID of the owner of the video', unique=False)
    liker_id = models.BigIntegerField('Liker ID', help_text='ID of the user liking the video')

    def __unicode__(self):
        return '{0}'.format(self.like_id)

    class Meta:
        db_table = 'likes'
        verbose_name = 'Like'
        verbose_name_plural = 'Likes'
