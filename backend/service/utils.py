#!/usr/bin/env python
from Crypto.Hash import SHA
import sys

sys.path.insert(1, '../')
from backend.settings import privateKey

def tokenize(timestamp, privateKey, vine_id):
    token = SHA.new()
    token.update(str(timestamp) + str(privateKey) + str(vine_id))
    return token.hexdigest()

if __name__ == '__main__':
    timestamp = raw_input("Enter the time stamp: ")
    vine_id = raw_input("Enter the vine_id: ")
    print "The token is: "
    print tokenize(timestamp, privateKey, vine_id)