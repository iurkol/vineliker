# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Player.id'
        db.delete_column('player', u'id')


        # Changing field 'Player.vine_id'
        db.alter_column('player', 'vine_id', self.gf('django.db.models.fields.BigIntegerField')(unique=True, primary_key=True))

    def backwards(self, orm):
        # Adding field 'Player.id'
        db.add_column('player', u'id',
                      self.gf('django.db.models.fields.AutoField')(default=0, primary_key=True),
                      keep_default=False)


        # Changing field 'Player.vine_id'
        db.alter_column('player', 'vine_id', self.gf('django.db.models.fields.CharField')(max_length=128, unique=True))

    models = {
        u'service.likes': {
            'Meta': {'object_name': 'Likes', 'db_table': "'likes'"},
            'like_id': ('django.db.models.fields.AutoField', [], {'unique': 'True', 'primary_key': 'True'}),
            'liker_id': ('django.db.models.fields.BigIntegerField', [], {}),
            'owner_id': ('django.db.models.fields.BigIntegerField', [], {}),
            'video_id': ('django.db.models.fields.BigIntegerField', [], {})
        },
        u'service.player': {
            'Meta': {'ordering': "['-date_added']", 'object_name': 'Player', 'db_table': "'player'"},
            'coins_spent_limit': ('django.db.models.fields.BigIntegerField', [], {'default': '100'}),
            'coins_spent_today': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'image_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'nmbr_coins': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'nmbr_coins_pfollow': ('django.db.models.fields.BigIntegerField', [], {'default': '5'}),
            'pro': ('django.db.models.fields.BooleanField', [], {}),
            'vine_id': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True', 'primary_key': 'True'})
        },
        u'service.playerfollow': {
            'Meta': {'ordering': "['-timestamp_follow']", 'object_name': 'PlayerFollow', 'db_table': "'follows'"},
            'follow_id': ('django.db.models.fields.AutoField', [], {'unique': 'True', 'primary_key': 'True'}),
            'followee_id': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'followee_id'", 'to': u"orm['service.Player']"}),
            'follower_id': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'follower_id'", 'to': u"orm['service.Player']"}),
            'timestamp_follow': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'service.promotedplayer': {
            'Meta': {'ordering': "['-date_promoted']", 'object_name': 'PromotedPlayer', 'db_table': "'promoted_players'"},
            'date_promoted': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'followers_remain': ('django.db.models.fields.BigIntegerField', [], {}),
            'image_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'vine_id': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True', 'primary_key': 'True'})
        },
        u'service.promotedvideos': {
            'Meta': {'ordering': "['-date_promoted']", 'object_name': 'PromotedVideos', 'db_table': "'promoted_videos'"},
            'date_promoted': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'number_of_likes': ('django.db.models.fields.BigIntegerField', [], {}),
            'thumbnail_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'video_id': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True', 'primary_key': 'True'}),
            'video_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'vine_id': ('django.db.models.fields.BigIntegerField', [], {})
        }
    }

    complete_apps = ['service']