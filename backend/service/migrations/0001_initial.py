# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Player'
        db.create_table('player', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vine_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=128)),
            ('image_url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('pro', self.gf('django.db.models.fields.BooleanField')()),
            ('nmbr_coins', self.gf('django.db.models.fields.BigIntegerField')(default=0)),
            ('nmbr_coins_pfollow', self.gf('django.db.models.fields.BigIntegerField')(default=5)),
            ('coins_spent_today', self.gf('django.db.models.fields.BigIntegerField')(default=0)),
            ('coins_spent_limit', self.gf('django.db.models.fields.BigIntegerField')(default=100)),
            ('date_added', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'service', ['Player'])

        # Adding model 'PromotedPlayer'
        db.create_table('promoted_players', (
            ('vine_id', self.gf('django.db.models.fields.BigIntegerField')(unique=True, primary_key=True)),
            ('image_url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('followers_remain', self.gf('django.db.models.fields.BigIntegerField')()),
            ('date_promoted', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'service', ['PromotedPlayer'])

        # Adding model 'PromotedVideos'
        db.create_table('promoted_videos', (
            ('video_id', self.gf('django.db.models.fields.BigIntegerField')(unique=True, primary_key=True)),
            ('vine_id', self.gf('django.db.models.fields.BigIntegerField')()),
            ('video_url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('thumbnail_url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('number_of_likes', self.gf('django.db.models.fields.BigIntegerField')()),
            ('date_promoted', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'service', ['PromotedVideos'])

        # Adding model 'PlayerFollow'
        db.create_table('follows', (
            ('follow_id', self.gf('django.db.models.fields.AutoField')(unique=True, primary_key=True)),
            ('followee_id', self.gf('django.db.models.fields.related.ForeignKey')(related_name='followee_id', to=orm['service.Player'])),
            ('follower_id', self.gf('django.db.models.fields.related.ForeignKey')(related_name='follower_id', to=orm['service.Player'])),
            ('timestamp_follow', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'service', ['PlayerFollow'])

        # Adding model 'Likes'
        db.create_table('likes', (
            ('like_id', self.gf('django.db.models.fields.AutoField')(unique=True, primary_key=True)),
            ('video_id', self.gf('django.db.models.fields.BigIntegerField')()),
            ('owner_id', self.gf('django.db.models.fields.BigIntegerField')()),
            ('liker_id', self.gf('django.db.models.fields.BigIntegerField')()),
        ))
        db.send_create_signal(u'service', ['Likes'])


    def backwards(self, orm):
        # Deleting model 'Player'
        db.delete_table('player')

        # Deleting model 'PromotedPlayer'
        db.delete_table('promoted_players')

        # Deleting model 'PromotedVideos'
        db.delete_table('promoted_videos')

        # Deleting model 'PlayerFollow'
        db.delete_table('follows')

        # Deleting model 'Likes'
        db.delete_table('likes')


    models = {
        u'service.likes': {
            'Meta': {'object_name': 'Likes', 'db_table': "'likes'"},
            'like_id': ('django.db.models.fields.AutoField', [], {'unique': 'True', 'primary_key': 'True'}),
            'liker_id': ('django.db.models.fields.BigIntegerField', [], {}),
            'owner_id': ('django.db.models.fields.BigIntegerField', [], {}),
            'video_id': ('django.db.models.fields.BigIntegerField', [], {})
        },
        u'service.player': {
            'Meta': {'ordering': "['-date_added']", 'object_name': 'Player', 'db_table': "'player'"},
            'coins_spent_limit': ('django.db.models.fields.BigIntegerField', [], {'default': '100'}),
            'coins_spent_today': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'nmbr_coins': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'nmbr_coins_pfollow': ('django.db.models.fields.BigIntegerField', [], {'default': '5'}),
            'pro': ('django.db.models.fields.BooleanField', [], {}),
            'vine_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'})
        },
        u'service.playerfollow': {
            'Meta': {'ordering': "['-timestamp_follow']", 'object_name': 'PlayerFollow', 'db_table': "'follows'"},
            'follow_id': ('django.db.models.fields.AutoField', [], {'unique': 'True', 'primary_key': 'True'}),
            'followee_id': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'followee_id'", 'to': u"orm['service.Player']"}),
            'follower_id': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'follower_id'", 'to': u"orm['service.Player']"}),
            'timestamp_follow': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'service.promotedplayer': {
            'Meta': {'ordering': "['-date_promoted']", 'object_name': 'PromotedPlayer', 'db_table': "'promoted_players'"},
            'date_promoted': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'followers_remain': ('django.db.models.fields.BigIntegerField', [], {}),
            'image_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'vine_id': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True', 'primary_key': 'True'})
        },
        u'service.promotedvideos': {
            'Meta': {'ordering': "['-date_promoted']", 'object_name': 'PromotedVideos', 'db_table': "'promoted_videos'"},
            'date_promoted': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'number_of_likes': ('django.db.models.fields.BigIntegerField', [], {}),
            'thumbnail_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'video_id': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True', 'primary_key': 'True'}),
            'video_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'vine_id': ('django.db.models.fields.BigIntegerField', [], {})
        }
    }

    complete_apps = ['service']