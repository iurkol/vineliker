# VineLiker.service.url
from django.conf.urls import patterns, include, url
from rest_framework.urlpatterns import format_suffix_patterns
from service import views

urlpatterns = patterns('',
    url(r'^players/$', views.player_list),
    url(r'^players/(?P<pk>[0-9]+)/$', views.player_detail),
    url(r'^ppls/$', views.promoted_player_list),
    url(r'^ppls/(?P<pk>[0-9]+)/$', views.promoted_player_detail),
    url(r'^videos/$', views.video_list),
    url(r'^videos/(?P<pk>[0-9]+)/$', views.video_detail),
    url(r'^likes/$', views.likes_view),
    url(r'^follows/$', views.follows_view),
    url(r'^upgrades/$', views.upgrades),
)

urlpatterns = format_suffix_patterns(urlpatterns)

urlpatterns += patterns('',
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
)