from django.forms import widgets
from rest_framework import serializers
from service.models import Player, PlayerFollow, PromotedPlayer, PromotedVideos, Likes

# Does not contain 'id' field as 'vine_id' is the model's primary key


class StringIDField(serializers.RelatedField):

    def to_native(self, value):
        return str(value)


class PlayerSerializer(serializers.ModelSerializer):
    #vine_id = StringIDField()

    class Meta:
        model = Player
        fields = ['vine_id', 'image_url', 'pro', 'nmbr_coins', 'nmbr_coins_pfollow',
                  'coins_spent_today', 'coins_spent_limit', 'date_added']

# Does not contain 'id' field as 'vine_id' is the model's primary key


class PromotedPlayerSerializer(serializers.ModelSerializer):
    vine_id = StringIDField()

    class Meta:
        model = PromotedPlayer
        fields = ['vine_id', 'image_url', 'followers_remain', 'date_promoted']


class PromotedVideosSerializer(serializers.ModelSerializer):
    vine_id = StringIDField()

    class Meta:
        model = PromotedVideos
        fields = ['vine_id', 'video_id', 'video_url', 'thumbnail_url', 'number_of_likes']


class LikesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Likes
        fields = ['video_id', 'owner_id', 'liker_id']


class PlayerFollowSerializer(serializers.ModelSerializer):

    class Meta:
        model = PlayerFollow
        fields = ['follower_id', 'followee_id']
