from django.contrib import admin
from service.models import Player, PromotedPlayer, PlayerFollow, PromotedVideos, Likes

class PlayerAdmin(admin.ModelAdmin):
    readonly_fields = ("date_added",)
    list_display = ('vine_id', 'image_url', 'pro', 'nmbr_coins', 'nmbr_coins_pfollow', 'coins_spent_today', 'coins_spent_limit', 'date_added')
    search_fields = ('vine_id', 'image_url', 'nmbr_coins', 'nmbr_coins_pfollow', 'coins_spent_today', 'coins_spent_limit')
    list_filter = ('date_added',)
    
class PromotedPlayerAdmin(admin.ModelAdmin):
    readonly_fields = ("date_promoted",)
    list_display = ('vine_id',  'image_url', 'followers_remain',  'date_promoted')
    search_fields = ('vine_id',  'image_url', 'followers_remain',  'date_promoted')
    list_filter = ('date_promoted',)
    
class PlayerFollowAdmin(admin.ModelAdmin):
    readonly_fields = ("timestamp_follow",)
    list_display = ("follow_id", "followee_id", "follower_id", "timestamp_follow",)
    search_fields = ("follow_id", "follower", "followee", "timestamp_follow",)
    list_filter = ('timestamp_follow',)
    
class PromotedVideosAdmin(admin.ModelAdmin):
    readonly_fields = ("date_promoted",)
    list_display = search_fields = ('video_id', 'vine_id', 'video_url', 'thumbnail_url', 'number_of_likes')
    
class LikesAdmin(admin.ModelAdmin):
    list_display = search_fields = ('like_id', 'video_id', 'owner_id', 'liker_id')

    
admin.site.register(Player, PlayerAdmin)
admin.site.register(PromotedPlayer, PromotedPlayerAdmin)
admin.site.register(PlayerFollow, PlayerFollowAdmin)
admin.site.register(PromotedVideos, PromotedVideosAdmin)
admin.site.register(Likes, LikesAdmin)