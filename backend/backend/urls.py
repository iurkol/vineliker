# Vine Liker root url
from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^', include('service.urls')),

    url(r'^area51/', include(admin.site.urls)),
)
